///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
/// 
///
/// @author Dane Takenaka <daneht@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   3/29/2021
///////////////////////////////////////////////////////////////////////////////
#pragma once
#include "node.hpp"
using namespace std;

namespace animalfarm{
   class SingleLinkedList {
      protected:
         Node* head = nullptr;
      public:
         bool empty();
         void push_front( Node* newNode );
         const unsigned int size() ;
         Node* get_first() const;
         Node* get_next( const Node* currentNode ) const;
         Node* pop_front();
   };
}

