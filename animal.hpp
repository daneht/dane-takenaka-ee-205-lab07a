///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Dane Takenaka <daneht@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/17/2021
//
//
#include <string>
#include "node.hpp"
#include "list.hpp"
#pragma once

#define RANDOM_NAME     Animal::getRandomName()
#define RANDOM_COLOR    Animal::getRandomColor()
#define RANDOM_GENDER   Animal::getRandomGender()
#define RANDOM_BOOL     Animal::getRandomBool()
#define RANDOM_WEIGHT   Animal::getRandomWeight( rand(), rand() )



using namespace std;

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, SILVER, YELLOW, BROWN }; 

class Animal : public Node {
public:
	enum Gender gender;
	string species;

	virtual const string speak() = 0;
	
	void printInfo();
	
	string colorName  (enum Color color);
	string genderName (enum Gender gender);

	static const         string getRandomName();
	static int           randomLowercaseLetterFromAtoZ();
	static int           randomUppercaseLetterFromAtoZ();

	static const Gender	getRandomGender();
	
	static const Color	getRandomColor();
	
	static const bool    getRandomBool();
	
	static const float	getRandomWeight( const float from, const float to );

   Animal();
   ~Animal();

};


class AnimalFactory {
public:
   static Animal* getRandomAnimal();

};


}
