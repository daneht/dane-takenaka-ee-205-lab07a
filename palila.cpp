///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Dane Takenaka <daneht@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/19/2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {

Palila::Palila( string found, enum Color newColor, enum Gender newGender ) {
   gender = newGender;         /// Get from the constructor... not all cats are the same gender (this is a has-a relationship)
   species = "Loxioides bailleui";    /// Hardcode this... all cats are the same species (this is a is-a relationship)
   featherColor = newColor;       /// A has-a relationship, so it comes through the constructor
   isMigratory  = false;    /// An is-a relationship, so it's safe to hardcode.  All cats have the same gestation period
   whereFound = found;
}


void Palila::printInfo() {
   cout << "Palila" << endl;
   cout << "   Where Found = [" << whereFound << "]" << endl;
   Bird::printInfo();
}

}

