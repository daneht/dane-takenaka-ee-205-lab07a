///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Laba 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// 
///
/// @author Dane Takenaka <daneht@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   3/29/2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <list>
#include "list.hpp"
#include "node.hpp"
using namespace std;
namespace animalfarm {


   bool SingleLinkedList::empty() {
      if (head == nullptr)
         return 1;
      else
         return 0;
   }


   void SingleLinkedList::push_front( Node* newNode ) {
      newNode -> next = head;
      head = newNode;
   }


      const unsigned int SingleLinkedList::size() {
      unsigned int sum = 0;
      Node* temp = head;
      for(sum == 0; temp != nullptr; sum++) {
         temp = temp -> next;
      }
      return sum;
}
      
   Node* SingleLinkedList::get_first() const {
      return head;
   }

   Node* SingleLinkedList::get_next( const Node* currentNode ) const {
      const Node* temp = currentNode;
      Node* nextNode = temp -> next;
      return nextNode;
   }

   Node* SingleLinkedList::pop_front() { 
      if(head ==  nullptr){
         return nullptr;
      }
         Node *temp = head;
         head = head -> next;
         return temp; 
   
   }

}
