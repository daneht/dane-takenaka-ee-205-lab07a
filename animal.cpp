///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Dane Takenaka <daneht@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/17/2021
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>

#include "animal.hpp"

#include "cat.hpp"
#include "dog.hpp"
#include "fish.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"


using namespace std;

namespace animalfarm {

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}

string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male");      break;
      case FEMALE:  return string("Female");    break;
      case UNKNOWN: return string("Unknown");   break;
   }

   return string("Really, really Unknown");
};

string Animal::colorName (enum Color color) {
   switch (color) {
      case RED:		return string("Red");      break;
      case YELLOW:	return string("Yellow");   break;
      case WHITE:	   return string("White");    break;
      case BLACK:	   return string("Black");    break;
      case BROWN:	   return string("Brown");    break;
      case SILVER:	return string("Silver");   break;
   }

   return string("Unknown");
};

int Animal::randomUppercaseLetterFromAtoZ() {
   int uppercase = 65 + rand() % (90 - 65 + 1);

   return uppercase;
}

int Animal::randomLowercaseLetterFromAtoZ() {
   int lowercase = 97 + rand() % (122 - 97 + 1);

   return lowercase;
}

const string Animal::getRandomName() {
   int length = 4 + rand() % (9 - 4 + 1);
   std::string randomname;

   randomname.insert( 0, 1, randomUppercaseLetterFromAtoZ() );
   for ( int i = 1; i < length; i++) {
      randomname.insert( i, 1, randomLowercaseLetterFromAtoZ() );	
   }

   return randomname;
}

const Gender Animal::getRandomGender() {
   int randomgender = rand() % ( 1 + 1 );
   Gender gender;

   switch (randomgender) {
      case 0:	gender = MALE;		break;
      case 1:	gender = FEMALE;	break;
   }

   return gender;
}

const Color Animal::getRandomColor() {
   int randomcolor = rand() % ( 9 );
   Color color;

   switch (randomcolor) {
      case 0:   color = RED;     break;
      case 1:   color = YELLOW;  break;
      case 2:   color = WHITE;   break;
      case 3:   color = BLACK;   break;
      case 4:   color = BROWN;   break;
      case 5:   color = SILVER;  break;
   }

   return color;
}

const bool Animal::getRandomBool() {
   bool randombool = rand() % ( 2 );

   return randombool;

}

const float Animal::getRandomWeight( const float from, const float to ) {

   float randomweight = ((float) rand()) / (float) RAND_MAX;
   float wieghtdifference = to - from;
   float r = randomweight * wieghtdifference;

   return from + r;
}

Animal* AnimalFactory::getRandomAnimal() {
   Animal* newAnimal = NULL;
   int i = rand() % ( 6 ); 
   switch (i) {
      case 0:  newAnimal = new Cat     ( RANDOM_NAME, RANDOM_COLOR, RANDOM_GENDER );   break;
      case 1:  newAnimal = new Dog     ( RANDOM_NAME, RANDOM_COLOR, RANDOM_GENDER );   break;
      case 2:  newAnimal = new Nunu    ( RANDOM_BOOL, RED, RANDOM_GENDER );            break;
      case 3:  newAnimal = new Aku     ( RANDOM_WEIGHT, SILVER, RANDOM_GENDER );       break;
      case 4:  newAnimal = new Palila  ( RANDOM_NAME, YELLOW, RANDOM_GENDER );         break;
      case 5:  newAnimal = new Nene    ( RANDOM_NAME, BROWN, RANDOM_GENDER );          break;
   }
   
   return newAnimal;
}

Animal::Animal(void) {     //constructor
   cout << ".";
}

Animal::~Animal(void) {    //destructor
   cout << "x";
}


}
