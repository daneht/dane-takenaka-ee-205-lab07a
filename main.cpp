///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// Dane Takenaka <daneht@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   3/20/2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <unistd.h>
#include <list>


#include "animal.hpp"
#include "node.hpp"
#include "list.hpp"


using namespace std;
using namespace animalfarm;

int main() {
   cout << "Welcome to Animal Farm 4" << endl;

   SingleLinkedList animalList; // Instantiate a SingleLinkedList
   for( auto i = 0 ; i < 25 ; i++ ) {
      animalList.push_front( AnimalFactory::getRandomAnimal() ) ;
   }
   cout << endl;
   cout << "List of Animals" << endl;
   cout << " Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << " Number of elements: " << animalList.size() << endl; 
   for( auto animal = animalList.get_first() // for() initialize
    ; animal != nullptr // for() test
    ; animal = animalList.get_next( animal )) { // for() increment
      cout << ((Animal*)animal)->speak() << endl;
   }
   while( !animalList.empty() ) {
      Animal* animal = (Animal*) animalList.pop_front();
   delete animal;
   }

return 0;
} 

